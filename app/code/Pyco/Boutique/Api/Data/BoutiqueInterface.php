<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Interface
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Api\Data;

interface BoutiqueInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const BOUTIQUE_ID       = 'boutique_id';
    const BOUTIQUE_NAME     = 'boutique_name';
    const ADDRESS           = 'address';
    const DISTRICT           = 'district';
    const CITY           = 'city';
    const REGION           = 'region';
    const COUNTRY           = 'country';
    const CONTACT           = 'contact';
    const MANAGER           = 'manager';
    const LAT               = 'lat';
    const LNG               = 'lng';
    const CREATION_TIME     = 'creation_time';
    const UPDATE_TIME       = 'update_time';
    const IS_ACTIVE         = 'is_active';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get boutique name
     *
     * @return string|null
     */
    public function getBoutiqueName();

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get district
     *
     * @return string|null
     */
    public function getDistrict();

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get region
     *
     * @return string|null
     */
    public function getRegion();

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get contact
     *
     * @return string|null
     */
    public function getContact();

    /**
     * Get manager
     *
     * @return string|null
     */
    public function getManager();

    /**
     * Get lat
     *
     * @return float|null
     */
    public function getLat();

    /**
     * Get long
     *
     * @return float|null
     */
    public function getLng();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setId($id);

    /**
     * Set boutique name
     *
     * @param string $boutiqueName
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setBoutiqueName($boutiqueName);

    /**
     * Set address
     *
     * @param string $address
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setAddress($address);

    /**
     * Set district
     *
     * @param string $district
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setDistrict($district);

    /**
     * Set city
     *
     * @param string $city
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setCity($city);

    /**
     * Set region
     *
     * @param string $region
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setRegion($region);

    /**
     * Set country
     *
     * @param string $country
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setCountry($country);

    /**
     * Set Contact
     *
     * @param string $contact
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setContact($contact);

    /**
     * Set Manager
     *
     * @param string $manager
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setManager($manager);

    /**
     * Set Lat
     *
     * @param float $lat
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setLat($lat);

    /**
     * Set Long
     *
     * @param float $lng
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setLng($lng);

    /**
     * Set CreationTime
     *
     * @param string $creationTime
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set UpdateTime
     *
     * @param string $updateTime
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set Is Active
     *
     * @param bool $isActive
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setIsActive($isActive);
}