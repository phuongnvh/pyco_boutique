<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Model
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Model;

use Pyco\Boutique\Api\Data\BoutiqueInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Boutique extends \Magento\Framework\Model\AbstractModel implements BoutiqueInterface, IdentityInterface
{
    /**
     * Boutique's statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = "pyco_boutique";

    /**
     * @var string
     */
    protected $_cacheTag = "pyco_boutique";

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = "pyco_boutique";

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Pyco\Boutique\Model\ResourceModel\Boutique');
    }

    /**
     * Prepare Boutique's statuses.
     * Available event pyco_boutique_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    public function checkCountry($country){
        return $this->_getResource()->checkCountry($country);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::BOUTIQUE_ID);
    }

    /**
     * Get boutique name
     *
     * @return string|null
     */
    public function getBoutiqueName()
    {
        return $this->getData(self::BOUTIQUE_NAME);
    }

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * Get district
     *
     * @return string|null
     */
    public function getDistrict()
    {
        return $this->getData(self::DISTRICT);
    }

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * Get region
     *
     * @return string|null
     */
    public function getRegion()
    {
        return $this->getData(self::REGION);
    }

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * Get contact
     *
     * @return string|null
     */
    public function getContact()
    {
        return $this->getData(self::CONTACT);
    }

    /**
     * Get manager
     *
     * @return string|null
     */
    public function getManager()
    {
        return $this->getData(self::MANAGER);
    }

    /**
     * Get lat
     *
     * @return float|null
     */
    public function getLat()
    {
        return $this->getData(self::LAT);
    }

    /**
     * Get long
     *
     * @return float|null
     */
    public function getLng()
    {
        return $this->getData(self::LNG);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }
    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setId($id)
    {
        return $this->setData(self::BOUTIQUE_ID, $id);
    }
    /**
     * Set boutique name
     *
     * @param string $boutiqueName
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setBoutiqueName($boutiqueName)
    {
        return $this->setData(self::BOUTIQUE_NAME, $boutiqueName);
    }

    /**
     * Set address
     *
     * @param string $address
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Set district
     *
     * @param string $district
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setDistrict($district)
    {
        return $this->setData(self::DISTRICT, $district);
    }

    /**
     * Set city
     *
     * @param string $city
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setCity($city)
    {
        return $this->setData(self::ADDRESS, $city);
    }

    /**
     * Set region
     *
     * @param string $region
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setRegion($region)
    {
        return $this->setData(self::ADDRESS, $region);
    }

    /**
     * Set country
     *
     * @param string $country
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::ADDRESS, $country);
    }

    /**
     * Set Contact
     *
     * @param string $contact
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setContact($contact)
    {
        return $this->setData(self::CONTACT, $contact);
    }

    /**
     * Set Manager
     *
     * @param string $manager
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setManager($manager)
    {
        return $this->setData(self::MANAGER, $manager);
    }

    /**
     * Set Lat
     *
     * @param float $lat
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setLat($lat)
    {
        return $this->setData(self::LAT, $lat);
    }

    /**
     * Set Long
     *
     * @param float $lng
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setLng($lng)
    {
        return $this->setData(self::LNG, $lng);
    }

    /**
     * Set CreationTime
     *
     * @param string $creationTime
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * Set UpdateTime
     *
     * @param string $updateTime
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Set Is Active
     *
     * @param bool $isActive
     * @return \Pyco\Boutique\Api\Data\BoutiqueInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }
}