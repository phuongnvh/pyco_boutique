<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Model Is Active Class
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Model\Boutique\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var \Pyco\Boutique\Model\Boutique
     */
    protected $boutique;

    /**
     * IsActive constructor.
     * @param \Pyco\Boutique\Model\Boutique $boutique
     */
    public function __construct(\Pyco\Boutique\Model\Boutique $boutique)
    {
        $this->boutique = $boutique;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->boutique->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
