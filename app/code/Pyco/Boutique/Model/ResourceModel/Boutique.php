<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Resource Model
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Model\ResourceModel;

/**
 * Pyco Boutique MySql Resource
 * @package Pyco\Boutique\Model\ResourceModel
 */
class Boutique extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Boutique constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param string|null $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        $resourcePrefix = null
    )
    {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('pyco_boutique_boutique', 'boutique_id');
    }

    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {

        if (!$this->isValidBoutiqueLat($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The boutique lat contains letters.')
            );
        }

        if (!$this->isValidBoutiqueLng($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The boutique lng contains letters.')
            );
        }

        if ($object->isObjectNew() && $this->isBoutiqueExist($object)){
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The boutique is existed.')
            );
        }

        if ($object->isObjectNew() && !$object->hasCreationTime()) {
            $object->setCreationTime($this->_date->gmtDate());
        }

        $object->setUpdateTime($this->_date->gmtDate());

        return parent::_beforeSave($object);
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Pyco\Boutique\Model\Boutique $object
     * @return \Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {

            $select->where(
                'is_active = ?',
                1
            )->limit(
                1
            );
        }

        return $select;
    }

    /**
     * Retrieve load select with filter by country and activity
     *
     * @param string $country
     * @param int $isActive
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadByCountrySelect($country, $isActive = null)
    {
        $select = $this->getConnection()->select()->from(
            ['b' => $this->getMainTable()]
        )->where(
            'b.country = ?',
            $country
        );

        if (!is_null($isActive)) {
            $select->where('b.is_active = ?', $isActive);
        }

        return $select;
    }

    /**
     *  Check whether boutique lat is valid
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    protected function isValidBoutiqueLat(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^\s*-?\d*(\.\d*)?\s*$/', $object->getData('lat'));
    }

    /**
     *  Check whether boutique long is valid
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    protected function isValidBoutiqueLng(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^\s*-?\d*(\.\d*)?\s*$/', $object->getData('lng'));
    }

    protected function isBoutiqueExist(\Magento\Framework\Model\AbstractModel $object)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from($this->getMainTable(), 'boutique_id')->where('boutique_name = :boutique_name');

        $bind = [':boutique_name' => (string)$object->getData("boutique_name")];

        return $connection->fetchOne($select, $bind);
    }

    public function checkCountry($country){
        return array("result" => __("Go Check Country Function in Resource. Country %s", $country) );
    }
}