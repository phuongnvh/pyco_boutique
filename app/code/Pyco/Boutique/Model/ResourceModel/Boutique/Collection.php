<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Resource Model Collection
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Model\ResourceModel\Boutique;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = "boutique_id";

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Pyco\Boutique\Model\Boutique', 'Pyco\Boutique\Model\ResourceModel\Boutique');
    }
}