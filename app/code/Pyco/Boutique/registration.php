<?php
/**
 * PYCO Boutique Locator
 *
 * Show boutique location by using Google Map and Markers
 *
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Pyco_Boutique',
    __DIR__
);