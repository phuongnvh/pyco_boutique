<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Block
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Block;

use Pyco\Boutique\Api\Data\BoutiqueInterface;
use Pyco\Boutique\Model\ResourceModel\Boutique\Collection as BoutiqueCollection;

class Boutique extends \Magento\Framework\View\Element\Template implements \Magento\Framework\DataObject\IdentityInterface
{
    protected $_boutiqueCollectionFactory;

    /**
     * Boutique constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Pyco\Boutique\Model\ResourceModel\Boutique\CollectionFactory $boutiqueCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Pyco\Boutique\Model\ResourceModel\Boutique\CollectionFactory $boutiqueCollectionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_boutiqueCollectionFactory = $boutiqueCollectionFactory;
    }

    /**
     * @return \Pyco\Boutique\Model\ResourceModel\Boutique\Collection
     */
    public function getBoutiques()
    {
        if(!$this->hasData('boutiques')) {
            $boutiques = $this->_boutiqueCollectionFactory
                ->create()
                ->addFilter('is_active', 1)
                ->addOrder(
                    BoutiqueInterface::CREATION_TIME,
                    BoutiqueCollection::SORT_ORDER_DESC
                );
            $this->setData('boutiques', $boutiques);
        }

        return $this->getData('boutiques');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Pyco\Boutique\Model\Boutique::CACHE_TAG. '_' . 'list'];
    }
}
