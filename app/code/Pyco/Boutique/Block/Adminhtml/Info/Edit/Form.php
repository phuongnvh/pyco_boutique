<?php
/**
 * PYCO Boutique Locator Module
 *
 * Admin Boutique Info Edit Form Block
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Block\Adminhtml\Info\Edit;

/**
 * Adminhtml boutique info edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('info_form');
        $this->setTitle(__('Boutique Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Pyco\Boutique\Model\Boutique $model */
        $model = $this->_coreRegistry->registry('boutique_info');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('info_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getBoutiqueId()) {
            $fieldset->addField('boutique_id', 'hidden', ['name' => 'boutique_id']);
        }

        $fieldset->addField(
            'boutique_name',
            'text',
            ['name' => 'boutique_name', 'label' => __('Boutique Name'), 'title' => __('Boutique Name'), 'required' => true]
        );

        $fieldset->addField(
            'address',
            'text',
            [
                'name' => 'address',
                'label' => __('Address'),
                'title' => __('Address'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'district',
            'text',
            [
                'name' => 'district',
                'label' => __('District'),
                'title' => __('District'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'region',
            'text',
            [
                'name' => 'region',
                'label' => __('Region'),
                'title' => __('Region'),
                'required' => false,
            ]
        );

        $fieldset->addField(
            'country',
            'text',
            [
                'name' => 'country',
                'label' => __('Country'),
                'title' => __('Country'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'contact',
            'text',
            [
                'name' => 'contact',
                'label' => __('Contact'),
                'title' => __('Contact'),
                'style' => 'height:12em',
                'required' => true,
            ]
        );

        $fieldset->addField(
            'manager',
            'text',
            [
                'name' => 'manager',
                'label' => __('Manager'),
                'title' => __('Manager'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'lat',
            'text',
            [
                'name' => 'lat',
                'label' => __('Lat'),
                'title' => __('Lat'),
                'required' => true,
                'class' => 'validate-number'
            ]
        );

        $fieldset->addField(
            'lng',
            'text',
            [
                'name' => 'lng',
                'label' => __('Long'),
                'title' => __('Long'),
                'required' => true,
                'class' => 'validate-number'
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Enabled'), '0' => __('Disabled')]
            ]
        );
        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
