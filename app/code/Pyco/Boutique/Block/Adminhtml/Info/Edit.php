<?php
/**
 * PYCO Boutique Locator Module
 *
 * Admin Boutique Info Edit Block
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Block\Adminhtml\Info;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize boutique info edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'boutique_id';
        $this->_blockGroup = 'Pyco_Boutique';
        $this->_controller = 'adminhtml_info';

        parent::_construct();

        if ($this->_isAllowedAction('Pyco_Boutique::save')) {
            $this->buttonList->update('save', 'label', __('Save Boutique Info'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );
        } else {
            $this->buttonList->remove('save');
        }

        if ($this->_isAllowedAction('Pyco_Boutique::info_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Info'));
        } else {
            $this->buttonList->remove('delete');
        }
    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('boutique_info')->getId()) {
            return __("Edit Boutique '%1'", $this->escapeHtml($this->_coreRegistry->registry('boutique_info')->getBoutiqueName()));
        } else {
            return __('New Boutique');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('boutique/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
}
