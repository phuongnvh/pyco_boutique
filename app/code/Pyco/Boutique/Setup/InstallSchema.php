<?php
/**
 * PYCO Boutique Locator Module
 *
 * Boutique Install Script
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Review\Block\Adminhtml\Product\Edit\Tab;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('pyco_boutique_boutique'))
            ->addColumn(
                'boutique_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Boutique Id'
            )
            ->addColumn('boutique_name', Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null], 'Boutique Name')
            ->addColumn('address', Table::TYPE_TEXT, 255, ['nullable' => true, 'default' => null])
            ->addColumn('district', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('city', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('region', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('country', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('contact', Table::TYPE_TEXT, '1M', [], 'Boutique Contact')
            ->addColumn('manager', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('lat', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('lng', Table::TYPE_TEXT, 100, ['nullable' => true, 'default' => null])
            ->addColumn('is_active', Table::TYPE_SMALLINT, null, ['nullable' => true, 'default' => '1'], 'Is Boutique Active?')
            ->addColumn('creation_time', Table::TYPE_DATETIME, null, ['nullable' => true], 'Creation Time')
            ->addColumn('update_time', Table::TYPE_DATETIME, null, ['nullable' => true], 'Update Time')
            ->addIndex($installer->getIdxName('pyco_boutique', ['boutique_name']), ['boutique_name'])
            ->setComment('Pyco Boutique')
            ;

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}