<?php
/**
 * PYCO Boutique Locator Module
 *
 * Admin Boutique Info Index Controller
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Controller\Adminhtml\Info;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // TODO: Implement execute() method.

        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Pyco_Boutique::info');
        $resultPage->addBreadcrumb(__('Boutique Info'), __('Boutique Info'));
        $resultPage->addBreadcrumb(__('Manage Boutiques Info'),__('Mange Boutiques Info'));
        $resultPage->getConfig()->getTitle()->prepend(__('Boutiques Info'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Pyco_Boutique::info');
    }
}
