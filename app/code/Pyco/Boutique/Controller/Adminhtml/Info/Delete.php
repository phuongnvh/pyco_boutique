<?php
/**
 * PYCO Boutique Locator Module
 *
 * Admin Boutique Info Edit Controller
 *
 * Author: Joe Nguyen
 * Version: 0.1.0
 */

namespace Pyco\Boutique\Controller\Adminhtml\Info;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Pyco_Boutique::save');
    }

    /**
     * Delete Action
     *
     * @return $this|\Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('boutique_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Pyco\Boutique\Model\Post');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The boutique has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['boutique_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a boutique to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
